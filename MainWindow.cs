// MainWindow.cs created with MonoDevelop
// User: dtvonline at 13:05 2009. 05. 15.
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//
using System;
using System.IO;
using System.Xml;
using Gtk;

public partial class MainWindow: Gtk.Window
{	
	public string startBristol = "startBristol";
	public string adriver, ogain, onogain, igain, oningain, rate, onrate;
	public string mdriver, channel, onchannel;
	public string voices, onvoices, tcp, host, ontcp, onhost, onpreload, preload, period, onperiod;
	public string mode, param, easydriver;
	public string detune, ondetune, onpitch, pwd, onvelocity, velocity, onglide, glide;
	public string onharmo, harmo, onfract, fract, glwf, lwf, hwf, nwf, wwf, autoe, autoa;
	public string jsdelay, jspath, jsuuid, onjsdelay, onjspath, onjsuuid, onjsdisable;
	public string config;
	public string onladigui, onladiengine, onladiindex, ladiindex;
	public string onnrpgui, onnrpengine, onnrpglobal, onnrpsize, nrpsize;
	public string onstateasy, onstatadv, onmultieasy, onmultiadv, multieasy, multiadv;
	
	
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
		
		//read config files
		if (System.IO.File.Exists (System.IO.Directory.GetCurrentDirectory() + "/.monobristol.xml")){
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(System.IO.Directory.GetCurrentDirectory() + "/.monobristol.xml");

			XmlNodeList importNodes = xmlDocument.SelectNodes("/config/param");

			foreach (XmlNode actualNode in importNodes)
			{
				adriver = actualNode["adriver"].InnerText;
				ogain = actualNode["ogain"].InnerText;
				onogain = actualNode["onogain"].InnerText;
				igain = actualNode["igain"].InnerText;
				oningain = actualNode["oningain"].InnerText;
				rate = actualNode["rate"].InnerText;
				onrate = actualNode["onrate"].InnerText;
				mdriver = actualNode["mdriver"].InnerText;
				channel = actualNode["channel"].InnerText;
				onchannel = actualNode["onchannel"].InnerText;
				voices = actualNode["voices"].InnerText;
				onvoices = actualNode["onvoices"].InnerText;
				ontcp = actualNode["ontcp"].InnerText;
				tcp = actualNode["tcp"].InnerText;
				onhost = actualNode["onhost"].InnerText;
				host = actualNode["host"].InnerText;
				preload = actualNode["preload"].InnerText;
				onpreload = actualNode["onpreload"].InnerText;
				onperiod = actualNode["onperiod"].InnerText;
				period = actualNode["period"].InnerText;
				mode = actualNode["mode"].InnerText;
				easydriver = actualNode["easydriver"].InnerText;
				ondetune = actualNode["ondetune"].InnerText;
				detune = actualNode["detune"].InnerText;
				pwd = actualNode["pwd"].InnerText;
				onpitch = actualNode["onpitch"].InnerText;
				onvelocity = actualNode["onvelocity"].InnerText;
				velocity = actualNode["velocity"].InnerText;
				onglide = actualNode["onglide"].InnerText;
				glide = actualNode["glide"].InnerText;
				glwf = actualNode["glwf"].InnerText;
				lwf = actualNode["lwf"].InnerText;
				hwf = actualNode["hwf"].InnerText;
				nwf = actualNode["nwf"].InnerText;
				wwf = actualNode["wwf"].InnerText;
				onharmo = actualNode["onharmo"].InnerText;
				harmo = actualNode["harmo"].InnerText;
				onfract = actualNode["onfract"].InnerText;
				fract = actualNode["fract"].InnerText;
				autoe = actualNode["autoe"].InnerText;
				autoa = actualNode["autoa"].InnerText;
				jsdelay = actualNode["jsdelay"].InnerText;
				jspath = actualNode["jspath"].InnerText;
				jsuuid = actualNode["jsuuid"].InnerText;
				onjsdelay = actualNode["onjsdelay"].InnerText;
				onjspath = actualNode["onjspath"].InnerText;
				onjsuuid = actualNode["onjsuuid"].InnerText;
				onjsdisable = actualNode["onjsdisable"].InnerText;
				onladiengine = actualNode["onladiengine"].InnerText;
				onladigui = actualNode["onladigui"].InnerText;
				onladiindex = actualNode["onladiindex"].InnerText;
				ladiindex = actualNode["ladiindex"].InnerText;
				onnrpengine = actualNode["onnrpengine"].InnerText;
				onnrpglobal = actualNode["onnrpglobal"].InnerText;
				onnrpgui = actualNode["onnrpgui"].InnerText;
				onnrpsize = actualNode["onnrpsize"].InnerText;
				nrpsize = actualNode["nrpsize"].InnerText;
				onstateasy = actualNode["onstateasy"].InnerText;
				onstatadv = actualNode["onstatadv"].InnerText;
				onmultiadv = actualNode["onmultiadv"].InnerText;
				onmultieasy = actualNode["onmultieasy"].InnerText;
				multiadv = actualNode["multiadv"].InnerText;
				multieasy = actualNode["multieasy"].InnerText;
				
				
			//set parameters and update GUI
			if (adriver == "alsa"){
					rbaalsa.Click ();
				}
			if (adriver == "oss"){
					rbaoss.Click ();
				}
			if (adriver == "jack"){
					rbajack.Click ();
				}
			if (mdriver == "alsa"){
					rbmalsa.Click ();
				}
			if (mdriver == "oss"){
					rbmoss.Click ();
				}
			if (mdriver == "seq"){
					rbmseq.Click ();
				}
			if (mdriver == "jack"){
					rbmjack.Click ();
				}
			spingain.Value = Convert.ToDouble(ogain);
			spiningain.Value = Convert.ToDouble(igain);
			entryrate.Text = rate;
			spinchannel1.Value = Convert.ToDouble(channel);
			spinvoices1.Value = Convert.ToDouble(voices);
			entrytcp1.Text = tcp;
			entrytcphost.Text = host;
			spinpreload.Value = Convert.ToDouble(preload);
			entrycount.Text = period;
			spindetune.Value = Convert.ToDouble(detune);
			entryvelocity.Text = velocity;
			spinglide.Value = Convert.ToDouble(glide);
			spinpitch.Value = Convert.ToDouble(pwd);
			spinmultiadv.Value = Convert.ToDouble(multiadv);
			spinmultieasy.Value = Convert.ToDouble(multieasy);
			entryharmo.Text = harmo;
			entryfract.Text = fract;
			entryjsdelay.Text = jsdelay;
			entryjspath.Text = jspath;
			entryjsuuid.Text = jsuuid;
			entryladiindex.Text = ladiindex;
			entrynrpsize.Text = nrpsize;
			
			if (autoe ==  "1"){
					chautoeasy.Active = true;
				}
			if (autoa == "1"){
					chautoadv.Active = true;
				}
			if (lwf == "1"){
					chlwf.Active = true;
				}
			if (hwf == "1"){
					chhwf.Active = true;
				}
			if (glwf == "1"){
					chglwf.Active = true;
				}
			if (nwf == "1"){
					chnwf.Active = true;
				}
			if (wwf == "1"){
					chwwf.Active = true;
				}
			if (onjsdelay == "1"){
					chonjsdelay.Active = true;
				}
			if (onjsdisable == "1"){
					chonjsdisable.Active = true;
				}
			if (onjspath == "1"){
					chonjspath.Active = true;
				}
			if (onjsuuid == "1"){
					chonjsuuid.Active = true;
				}
			if (onladiengine == "1"){
					chonladiengine.Active = true;
				}
			if (onladigui == "1"){
					chonladigui.Active = true;
				}
			if (onladiindex == "1"){
					chonladiindex.Active = true;
				}
			if (onnrpengine == "1"){
					chonnrpengine.Active = true;
				}
			if (onnrpglobal == "1"){
					chonnrpglobal.Active = true;
				}
			if (onnrpgui == "1"){
					chonnrpgui.Active = true;
				}
			if (onnrpsize == "1"){
					chonnrpsize.Active = true;
				}
			if (ontcp == "1"){
					chontcpport.Active = true;
				}
			if (onhost == "1"){
					chontcphost.Active = true;
				}
			if (onvoices == "1"){
					chonvoices.Active = true;
				}
			if (onchannel == "1"){
					chonchannel.Active = true;
				}
			if (oningain == "1"){
					choningain.Active = true;
				}
			if (onogain == "1"){
					chonogain.Active = true;
				}
			if (onpitch == "1"){
					chonpitch.Active = true;
				}
			if (ondetune == "1"){
					chondetune.Active = true;
				}
			if (onperiod == "1"){
					chonperiod.Active = true;
				}
			if (onrate == "1"){
					chonrate.Active = true;
				}
			if (onpreload == "1"){
					chonpreload.Active = true;
				}
			if (onvelocity == "1"){
					chonvelocity.Active = true;
				}
			if (onglide == "1"){
					chonglide.Active = true;
				}
			if (onharmo == "1"){
					chonharmonics.Active = true;
				}
			if (onfract == "1"){
					chonfract.Active = true;
				}
			if (onmultieasy == "1"){
					chmultieasy.Active = true;
				}
			if (onmultiadv == "1"){
					chmultiadv.Active = true;
				}
			if (onstatadv == "1"){
					chstatadv.Active = true;
				}
			if (onstateasy == "1"){
					chstateasy.Active = true;
				}
				
				
			
			if (mode == "easy"){
					rbeasy.Click ();
				}
			if (mode == "advanced"){
					rbadv1.Click ();
				}
			if (easydriver == "alsa"){
					rbealsa.Click ();
				}
			if (easydriver == "oss"){
					rbeoss.Click ();
				}
			if (easydriver == "jack"){
					rbejack.Click ();
				}

			}
		}else{
			//generate config file
			bapply.Press ();
		}
	}
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	//Set synths parameters
	protected void SetMode ()
	{
		if (mode == "easy") {
			param = param + "-"+easydriver;
			if (easydriver == "alsa"){
				param = param+" -audiodev default ";
			} else if (easydriver == "jack"){
				if (autoe == "1"){
					param = param+" -autoconn";
				}
				if (onstateasy == "1"){
					param = param+" -jackstats";
				}
				if (onmultieasy == "1"){
					param = param+" -multi" + multieasy;
				}
				if (onjsdisable == "1"){
					param = param+" -session";
				}else{
					if (onjsdelay == "1"){
					param = param+" -jsmd " + jsdelay;
					}
					if (onjspath == "1"){
					param = param+" -jsmfile " + jspath;
					}
					if (onjsuuid == "1"){
					param = param+" -jsmuuid " + jsuuid;
					}
				}
			}
		}
		
		if (mode == "advanced") {
			param = param + "-audio "+adriver+" -midi "+mdriver;
			if (adriver == "alsa"){
				param = param+" -audiodev default ";
			} if (adriver == "jack"){
				if (autoa == "1"){
					param = param+" -autoconn";
				}
				if (onstatadv == "1"){
					param = param+" -jackstats";
				}
				if (onmultiadv == "1"){
					param = param+" -multi" + multiadv;
				}
				if (onjsdisable == "1"){
					param = param+" -session";
				}else{
					if (onjsdelay == "1"){
					param = param+" -jsmd " + jsdelay;
					}
					if (onjspath == "1"){
					param = param+" -jsmfile " + jspath;
					}
					if (onjsuuid == "1"){
					param = param+" -jsmuuid " + jsuuid;
					}
				}
			}
		}
		
		
		if (onfract == "1"){
			param = param+" -blofraction " + fract;
		}
		if (onharmo == "1"){
			param = param+" -blo " + harmo;
		}
		if (onglide == "1"){
			param = param+" -glide " + glide;
		}
		if (onvelocity == "1"){
			param = param+" -velocity " + velocity;
		}
		if (onpreload == "1"){
			param = param+" -preload " + preload;
		}
		if (onperiod == "1"){
			param = param+" -count " + period;
		}
		if (onrate == "1"){
			param = param+" -rate " + rate;
		}
		if (ondetune == "1"){
			param = param+" -detune " + detune;
		}
		if (onpitch == "1"){
			param = param+" -pwd " + pwd;
		}
		if (onogain == "1"){
			param = param+" -outgain " + ogain;
		}
		if (oningain == "1"){
			param = param+" -ingain " + igain;
		}
		if (onchannel == "1"){
			param = param+" -channel " + channel;
		}
		if (onvoices == "1"){
			param = param+" -voices " + voices;
		}
		if (onhost == "1"){
			param = param+" -host " + host;
		}
		if (ontcp == "1"){
			param = param+" -port " + tcp;
		}
		if (onnrpsize == "1"){
			param = param+" -nrpcc " + nrpsize;
		}
		if (onnrpgui == "1"){
			param = param+" -gnrp";
		}
		if (onnrpglobal == "1"){
			param = param+" -nrp";
		}
		if (onnrpengine == "1"){
			param = param+" -enrp";
		}
		if (onladiengine == "1"){
			param = param+" -ladi bristol";
		}
		if (onladigui == "1"){
			param = param+" -ladi brighton";
		}
		if (onladiindex == "1"){
			param = param+" -ladi " + ladiindex;
		}
		if (nwf == "1"){
			param = param+" -nwf";
		}
		if (lwf ==  "1"){
			param = param+" -lwf";
		}
		if (hwf ==  "1"){
			param = param+" -hwf";
		}
		if (glwf == "1"){
			param = param+" -glwf";
		}
		if (wwf == "1"){
			param = param+" -wwf";
		}
	}	
	
	protected virtual void OnBapplyPressed (object sender, System.EventArgs e)
	{
		//check parameters
		if (rbaalsa.Active.ToString () == "True") {
			adriver = "alsa";
		}
		if (rbaoss.Active.ToString () == "True") {
			adriver = "oss";
		}
		if (rbajack.Active.ToString () == "True") {
			adriver = "jack";
		}
		if (rbmalsa.Active.ToString () == "True") {
			mdriver = "alsa";
		}
		if (rbmoss.Active.ToString () == "True") {
			mdriver = "oss";
		}
		if (rbmseq.Active.ToString () == "True") {
			mdriver = "seq";
		}
		if (rbmjack.Active.ToString () == "True") {
			mdriver = "jack";
		}
		
		multiadv = spinmultiadv.Text.ToString ();
		multieasy = spinmultieasy.Text.ToString ();
		ogain = spingain.Text.ToString ();
		igain = spiningain.Text.ToString ();
		rate = entryrate.Text.ToString ();
		channel = spinchannel1.Text.ToString ();
		voices = spinvoices1.Text.ToString ();
		tcp = entrytcp1.Text.ToString ();
		host = entrytcphost.Text.ToString ();
		preload = spinpreload.Text.ToString ();
		period = entrycount.Text.ToString ();
		detune = spindetune.Text.ToString ();
		pwd = spinpitch.Text.ToString ();
		velocity = entryvelocity.Text.ToString ();
		glide = spinglide.Text.ToString ();
		harmo = entryharmo.Text.ToString ();
		fract = entryfract.Text.ToString ();
		jsdelay = entryjsdelay.Text.ToString ();
		jspath = entryjspath.Text.ToString ();
		jsuuid = entryjsuuid.Text.ToString ();
		ladiindex = entryladiindex.Text.ToString ();
		nrpsize = entrynrpsize.Text.ToString ();
		
		
		if (chautoeasy.Active.ToString () == "True"){
			autoe = "1";
		}else{
			autoe = "0";
		}
		if (chautoadv.Active.ToString () == "True"){
			autoa = "1";
		}else{
			autoa = "0";
		}
		if (chlwf.Active.ToString () == "True"){
			lwf = "1";
		}else{
			lwf = "0";
		}
		if (chhwf.Active.ToString () == "True"){
			hwf = "1";
		}else{
			hwf = "0";
		}
		if (chglwf.Active.ToString () == "True"){
			glwf = "1";
		}else{
			glwf = "0";
		}
		if (chnwf.Active.ToString () == "True"){
			nwf = "1";
		}else{
			nwf = "0";
		}
		if (chwwf.Active.ToString () == "True"){
			wwf = "1";
		}else{
			wwf = "0";
		}
		if (chonjsdelay.Active.ToString () == "True"){
			onjsdelay = "1";
		}else{
			onjsdelay = "0";
		}
		if (chonjsdisable.Active.ToString () == "True"){
			onjsdisable = "1";
		}else{
			onjsdisable = "0";
		}
		if (chonjspath.Active.ToString () == "True"){
			onjspath = "1";
		}else{
			onjspath = "0";
		}
		if (chonjsuuid.Active.ToString () == "True"){
			onjsuuid = "1";
		}else{
			onjsuuid = "0";
		}
		if (chonladiengine.Active.ToString () == "True"){
			onladiengine = "1";
		}else{
			onladiengine = "0";
		}
		if (chonladigui.Active.ToString () == "True"){
			onladigui = "1";
		}else{
			onladigui = "0";
		}
		if (chonladiindex.Active.ToString () == "True"){
			onladiindex = "1";
		}else{
			onladiindex = "0";
		}
		if (chonnrpengine.Active.ToString () == "True"){
			onnrpengine = "1";
		}else{
			onnrpengine = "0";
		}
		if (chonnrpglobal.Active.ToString () == "True"){
			onnrpglobal = "1";
		}else{
			onnrpglobal = "0";
		}
		if (chonnrpgui.Active.ToString () == "True"){
			onnrpgui = "1";
		}else{
			onnrpgui = "0";
		}
		if (chonnrpsize.Active.ToString () == "True"){
			onnrpsize = "1";
		}else{
			onnrpsize = "0";
		}
		if (chontcphost.Active.ToString () == "True"){
			ontcp = "1";
		}else{
			ontcp = "0";
		}
		if (chontcpport.Active.ToString () == "True"){
			onhost = "1";
		}else{
			onhost = "0";
		}
		if (chonvoices.Active.ToString () == "True"){
			onvoices = "1";
		}else{
			onvoices = "0";
		}
		if (chonchannel.Active.ToString () == "True"){
			onchannel = "1";
		}else{
			onchannel = "0";
		}
		if (choningain.Active.ToString () == "True"){
			oningain = "1";
		}else{
			oningain = "0";
		}
		if (chonogain.Active.ToString () == "True"){
			onogain = "1";
		}else{
			onogain = "0";
		}
		if (chondetune.Active.ToString () == "True"){
			ondetune = "1";
		}else{
			ondetune = "0";
		}
		if (chonpitch.Active.ToString () == "True"){
			onpitch = "1";
		}else{
			onpitch = "0";
		}
		if (chonperiod.Active.ToString () == "True"){
			onperiod = "1";
		}else{
			onperiod = "0";
		}
		if (chonpreload.Active.ToString () == "True"){
			onpreload = "1";
		}else{
			onpreload = "0";
		}
		if (chonvelocity.Active.ToString () == "True"){
			onvelocity = "1";
		}else{
			onvelocity = "0";
		}
		if (chonglide.Active.ToString () == "True"){
			onglide = "1";
		}else{
			onglide = "0";
		}
		if (chonharmonics.Active.ToString () == "True"){
			onharmo = "1";
		}else{
			onharmo = "0";
		}
		if (chonfract.Active.ToString () == "True"){
			onfract = "1";
		}else{
			onfract = "0";
		}
		if (chonrate.Active.ToString () == "True"){
			onrate = "1";
		}else{
			onrate = "0";
		}
		if (chstateasy.Active.ToString () == "True"){
			onstateasy = "1";
		}else{
			onstateasy = "0";
		}
		if (chstatadv.Active.ToString () == "True"){
			onstatadv = "1";
		}else{
			onstatadv = "0";
		}
		if (chmultiadv.Active.ToString () == "True"){
			onmultiadv = "1";
		}else{
			onmultiadv = "0";
		}
		if (chmultieasy.Active.ToString () == "True"){
			onmultieasy = "1";
		}else{
			onmultieasy = "0";
		}
		
		
		if (rbeasy.Active.ToString () == "True"){
			mode = "easy";
			rbeasy.Click ();
		}
		if (rbadv1.Active.ToString () == "True"){
			mode = "advanced";
			rbadv1.Click ();
		}
		if (rbealsa.Active.ToString () == "True") {
			easydriver = "alsa";
		}
		if (rbeoss.Active.ToString () == "True") {
			easydriver = "oss";
		}
		if (rbejack.Active.ToString () == "True") {
			easydriver = "jack";
		}
			
		//generate config file
		config = "<?xml version='1.0' encoding='UTF-8'?><config><param>";
		config = config + "<adriver>"+adriver+"</adriver><adevice></adevice><onogain>"+onogain+"</onogain><ogain>"+ogain+"</ogain><oningain>"+oningain+"</oningain><igain>"+igain+"</igain><onrate>"+onrate+"</onrate><rate>"+rate+"</rate><mdriver>"+mdriver+"</mdriver><mdevice></mdevice><onchannel>"+onchannel+"</onchannel><channel>"+channel+"</channel>";
		config = config + "<onperiod>"+onperiod+"</onperiod><period>"+period+"</period><mode>"+mode+"</mode><easydriver>"+easydriver+"</easydriver><ondetune>"+ondetune+"</ondetune><detune>"+detune+"</detune><onpitch>"+onpitch+"</onpitch><pwd>"+pwd+"</pwd><onvelocity>"+onvelocity+"</onvelocity><velocity>"+velocity+"</velocity><onglide>"+onglide+"</onglide><glide>"+glide+"</glide><onfract>"+onfract+"</onfract><fract>"+fract+"</fract><onharmo>"+onharmo+"</onharmo><harmo>"+harmo+"</harmo><lwf>"+lwf+"</lwf><hwf>"+hwf+"</hwf>";
		config = config + "<nwf>"+nwf+"</nwf><wwf>"+wwf+"</wwf><glwf>"+glwf+"</glwf><autoe>"+autoe+"</autoe><autoa>"+autoa+"</autoa>";
		config = config + "<onjsdelay>"+onjsdelay+"</onjsdelay><onjsdisable>"+onjsdisable+"</onjsdisable><onjspath>"+onjspath+"</onjspath><onjsuuid>"+onjsuuid+"</onjsuuid>";
		config = config + "<jsdelay>"+jsdelay+"</jsdelay><jspath>"+jspath+"</jspath><jsuuid>"+jsuuid+"</jsuuid>";
		config = config + "<onladiengine>"+onladiengine+"</onladiengine><onladigui>"+onladigui+"</onladigui><onladiindex>"+onladiindex+"</onladiindex><ladiindex>"+ladiindex+"</ladiindex>";
		config = config + "<onnrpengine>"+onnrpengine+"</onnrpengine><onnrpgui>"+onnrpgui+"</onnrpgui><onnrpglobal>"+onnrpglobal+"</onnrpglobal><onnrpsize>"+onnrpsize+"</onnrpsize><nrpsize>"+nrpsize+"</nrpsize>";
		config = config + "<onvoices>"+onvoices+"</onvoices><voices>"+voices+"</voices><tcp>"+tcp+"</tcp><host>"+host+"</host><onhost>"+onhost+"</onhost><ontcp>"+ontcp+"</ontcp><onpreload>"+onpreload+"</onpreload><preload>"+preload+"</preload>";
		config = config + "<onmultieasy>"+onmultieasy+"</onmultieasy><onmultiadv>"+onmultiadv+"</onmultiadv><onstatadv>"+onstatadv+"</onstatadv><onstateasy>"+onstateasy+"</onstateasy><multiadv>"+multiadv+"</multiadv><multieasy>"+multieasy+"</multieasy>";
		config = config + "</param></config>";
		System.IO.File.WriteAllText (System.IO.Directory.GetCurrentDirectory() + "/.monobristol.xml", config);
	}
	
	//start synths
	protected virtual void OnBmoogminiPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -mini");
	}

	protected virtual void OnBmoogvoyagerPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -explorer");
		
	}

	protected virtual void OnBvoybluePressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -voyager");
	}

	protected virtual void OnBmoogmemoPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -memory");
	}

	protected virtual void OnBmoogrealPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -mg1");
	}

	protected virtual void OnBsonicPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -sonic6");
	}

	protected virtual void OnBpro5Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -prophet");
	}

	protected virtual void OnBpro10Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -pro10");
	}

	protected virtual void OnBpro52Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -pro52");
	}

	protected virtual void OnBpro1Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -pro1");
	}

	protected virtual void OnBobxPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -obx");
	}

	protected virtual void OnBobxaPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -obxa");
	}

	protected virtual void OnBaxxePressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -axxe");
	}

	protected virtual void OnBodyPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -odyssey");
	}

	protected virtual void OnB2600Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -arp2600");
	}

	protected virtual void OnBsolinaPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -solina");
	}

	protected virtual void OnBjuno6Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -juno");
	}

	protected virtual void OnBjupiter8Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -jupiter");
	}

	protected virtual void OnBmonopolyPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -monopoly");
	}

	protected virtual void OnBms20Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -ms20");
	}

	protected virtual void OnBpoly800Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -poly800");
	}

	protected virtual void OnBpolysixPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -polysix");
	}

	protected virtual void OnBvoxconPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -vox");
	}

	protected virtual void OnBcon300Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -voxM2");
	}

	protected virtual void OnBb3Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -b3");
	}

	protected virtual void OnBmodulPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -hammond");
	}

	protected virtual void OnBrhode73Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -rhodes");
	}

	protected virtual void OnBrhodebassPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -rhodesbass");
	}

	protected virtual void OnBydxPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -dx");
	}

	protected virtual void OnBcs80Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -cs80");
	}

	protected virtual void OnBbit1Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -bitone");
	}

	protected virtual void OnBbit99Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -bit99");
	}

	protected virtual void OnBbit100Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -bit100");
	}

	protected virtual void OnBroadrunnerPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -roadrunner");
	}

	protected virtual void OnBstratusPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -stratus");
	}

	protected virtual void OnBtrilogyPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -trilogy");
	}

	protected virtual void OnBaksPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -aks");
	}

	protected virtual void OnBbme700Pressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -bme700");
	}

	protected virtual void OnBsidPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -sid");
	}

	protected virtual void OnBmixerPressed (object sender, System.EventArgs e)
	{
		SetMode ();
		System.Diagnostics.Process.Start (startBristol, param+" -mixer");
	}

	//set default settings
	protected virtual void OnBdefaultPressed (object sender, System.EventArgs e)
	{
		spingain.Value = 4;
		chonogain.Active = false;
		spiningain.Value = 4;
		choningain.Active = false;
		chonrate.Active = false;
		entryrate.Text = "44100";
		spinchannel1.Value = 1;
		chonchannel.Active = false;
		spinvoices1.Value = 16;
		chonvoices.Active = false;
		entrytcp1.Text = "5028";
		entrytcphost.Text = "localhost";
		spinpreload.Value = 8;
		chonpreload.Active = false;
		entrycount.Text = "256";
		spindetune.Value = 0;
		chondetune.Active = false;
		spinpitch.Value = 2;
		chonpitch.Active = false;
		chonglide.Active = false;
		spinglide.Value = 5;
		chonvelocity.Active = false;
		entryvelocity.Text = "510";
		chonharmonics.Active = false;
		entryharmo.Text = "0";
		chonfract.Active = false;
		entryfract.Text = "0.8";
		rbaalsa.Click ();
		rbeasy.Click ();
		rbealsa.Click ();
		rbmseq.Click ();
		bapply.Press ();
		chlwf.Active = false;
		chhwf.Active = false;
		chnwf.Active = false;
		chwwf.Active = false;
		chglwf.Active = false;
		chautoadv.Active = false;
		chautoeasy.Active = false;
		chonjsdelay.Active = false;
		chonjsdisable.Active = false;
		chonjspath.Active = false;
		chonjsuuid.Active = false;
		entryjsdelay.Text = "5000";
		entryjspath.Text = "";
		entryjsuuid.Text = "";
		entryladiindex.Text = "1024";
		chonladiengine.Active = false;
		chonladigui.Active = false;
		chonladiindex.Active = false;
		entrynrpsize.Text = "128";
		chonnrpengine.Active = false;
		chonnrpglobal.Active = false;
		chonnrpgui.Active = false;
		chonnrpsize.Active = false;
		chontcphost.Active = false;
		chontcpport.Active = false;
		chonperiod.Active = false;
		chonrate.Active = false;
		chmultieasy.Active = false;
		chmultiadv.Active = false;
		chstatadv.Active = false;
		chstateasy.Active = false;
		spinmultiadv.Value = 1;
		spinmultieasy.Value = 1;
		
	}

	//select mode
	protected virtual void OnRbeasyClicked (object sender, System.EventArgs e)
	{
		rbaalsa.Hide ();
		rbajack.Hide ();
		rbaoss.Hide ();
		rbmalsa.Hide ();
		rbmoss.Hide ();
		rbmseq.Hide ();
		rbmjack.Hide ();
		laudio.Hide ();
		lmidi.Hide ();
		rbealsa.Show ();
		rbeoss.Show ();
		rbejack.Show();
		chautoadv.Hide ();
		chautoeasy.Show ();
		chstatadv.Hide ();
		chmultiadv.Hide ();
		chstateasy.Show ();
		chmultieasy.Show ();
		spinmultiadv.Hide ();
		spinmultieasy.Show ();
	}

	protected virtual void OnRbadv1Clicked (object sender, System.EventArgs e)
	{
		rbaalsa.Show ();
		rbajack.Show ();
		rbaoss.Show ();
		rbmalsa.Show ();
		rbmoss.Show ();
		rbmseq.Show ();
		rbmjack.Show ();
		laudio.Show ();
		lmidi.Show ();
		rbealsa.Hide ();
		rbeoss.Hide ();
		rbejack.Hide ();
		chautoeasy.Hide ();
		chautoadv.Show ();
		chstatadv.Show ();
		chmultiadv.Show ();
		chstateasy.Hide ();
		chmultieasy.Hide ();
		spinmultiadv.Show ();
		spinmultieasy.Hide ();
	}

	protected virtual void OnChnwfToggled (object sender, System.EventArgs e)
	{
		if (chnwf.Active == true){
			chlwf.Active = false;
			chhwf.Active = false;
			chwwf.Active = false;
			chglwf.Active = false;
		}
	}

	protected virtual void OnChonjsdisableToggled (object sender, System.EventArgs e)
	{
		if (chonjsdisable.Active == true){
			chonjsdelay.Active = false;
			chonjspath.Active = false;
			chonjsuuid.Active = false;
		}
	}

	protected virtual void OnChonladiguiToggled (object sender, System.EventArgs e)
	{
		if (chonladigui.Active == true){
			chonladiengine.Active = false;
		}else{
			chonladiindex.Active = false;
		}
	}

	protected virtual void OnChonladiengineToggled (object sender, System.EventArgs e)
	{
		if (chonladiengine.Active == true){
			chonladigui.Active = false;
		}else{
			chonladiindex.Active = false;
		}
	}


	protected virtual void OnChonjsuuidToggled (object sender, System.EventArgs e)
	{
		if (chonjsuuid.Active == true){
			chonjsdisable.Active = false;
		}
	}

	protected virtual void OnChonjspathToggled (object sender, System.EventArgs e)
	{
		if (chonjspath.Active == true){
			chonjsdisable.Active = false;
		}
	}

	protected virtual void OnChonjsdelayToggled (object sender, System.EventArgs e)
	{
		if (chonjsdelay.Active == true){
			chonjsdisable.Active = false;
		}
	}

	protected virtual void OnChlwfToggled (object sender, System.EventArgs e)
	{
		if (chlwf.Active == true){
			chnwf.Active = false;
		}
	}

	protected virtual void OnChhwfToggled (object sender, System.EventArgs e)
	{
		if (chhwf.Active == true){
			chnwf.Active = false;
		}
	}

	protected virtual void OnChwwfToggled (object sender, System.EventArgs e)
	{
		if (chwwf.Active == true){
			chnwf.Active = false;
		}
	}

	protected virtual void OnChglwfToggled (object sender, System.EventArgs e)
	{
		if (chglwf.Active == true){
			chnwf.Active = false;
		}
	}
		

protected virtual void OnChonnrpguiToggled (object sender, System.EventArgs e)
{
		if (chonnrpgui.Active == true){
			chonnrpengine.Active = false;
			chonnrpglobal.Active = false;
		}else{
			chonnrpsize.Active = false;
		}
}

protected virtual void OnChonnrpengineToggled (object sender, System.EventArgs e)
{
		if (chonnrpengine.Active == true){
			chonnrpgui.Active = false;
			chonnrpglobal.Active = false;
		}else{
			chonnrpsize.Active = false;
		}
}

protected virtual void OnChonnrpglobalToggled (object sender, System.EventArgs e)
{
		if (chonnrpglobal.Active == true){
			chonnrpengine.Active = false;
			chonnrpgui.Active = false;
		}else{
			chonnrpsize.Active = false;
		}
}

protected virtual void OnChonladiindexToggled (object sender, System.EventArgs e)
{
		if (chonladiindex.Active == true){
			if (chonladiengine.Active == false && chonladigui.Active == false){
				chonladiindex.Active = false;
			}
		}
}

protected virtual void OnChonnrpsizeToggled (object sender, System.EventArgs e)
{
		if (chonnrpsize.Active == true){
			if (chonnrpengine.Active == false && chonnrpglobal.Active == false && chonnrpgui.Active == false){
				chonnrpsize.Active = false;
			}
		}
}
}
